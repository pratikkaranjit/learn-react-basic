import React, { useEffect, useState } from "react";

const LearnUseEffect2 = () => {
  let [count, setCount] = useState(0);
  useEffect(() => {
    //This clg runs only after tala ko return function is completed 
    console.log("i am asynchronous useEffect");

    //useEffect ko bhitra return chalako cha bhane function dinu paryo 
    //Return bhitrako code first ma run huncha ani matra mathi ko clg run huncha 
    return () => {                               
      console.log("i am return");
    };
  }, [count]);

  //This code runs in first render as it is outside of any asynchronous function 

  console.log("i am first");
  return (
    <div>
      LearnUseEffect2
      <br></br>
      {count}
      <br></br>
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >change count1
      </button>
    </div>
  );
};
export default LearnUseEffect2;