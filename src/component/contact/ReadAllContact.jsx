import React, { useEffect, useState } from 'react'
import { baseUrl } from '../../config/config'
import axios from 'axios'

const ReadAllContact = () => {

    let [contacts,setContact] = useState([])

    let readAllContact = async() => {
        let info = {
            url: `${baseUrl}/contacts`,
            method: "get",
        }

        try {
            let result = await axios(info)
            setContact(result.data.data.results)
        } catch (error) {
            console.log(error.message)
        }
        
    }
    useEffect(() => {
        readAllContact();
    },[])
    console.log(contacts)

    let deleteContact = async(_id) => {
        let info1 = {
            url: `${baseUrl}/contacts/${_id}`,
            method: "delete",
        }

        let result = await axios(info1)
    }

  return (
    <div>
        {contacts.map((item,i) => {
            return (
                <div key = {i} style={{border: "solid red 3px"}}>
                    <p>Full name:{item.fullName}</p>
                    <p>Address: {item.address}</p>
                    <p>Phone Number: {item.phoneNumber}</p>
                    <p>email: {item.email}</p>
                    <button
                        onClick={async() => {
                            await deleteContact(item._id);
                            await readAllContact();
                        }}
                    >delete</button>
                </div>
            )
        })}
    </div>
  )
}

export default ReadAllContact