import React, { useState } from 'react'
import { baseUrl } from '../../config/config'
import axios from 'axios'

const Practice = () => {

    let [name,setName] = useState("")
    let [phone,setPhone] = useState("")
    let [address,setAddress] = useState("")
    let [country,setCountry] = useState("")
    let [email,setEmail] = useState("")


    let countries = [
        { label: "Select Country", value: "", disabled: true },
        { label: "Nepal", value: "nepal" },
        { label: "China", value: "china" },
        { label: "India", value: "india" },
        { label: "America", value: "america" },
      ];

    
  return (
        <>
        <div className='myForm'>
        <p style={{ fontFamily: 'Arial', paddingLeft: '10rem' }}> <b>My Form </b></p>
        <form 
        onSubmit={async(e) => {
            e.preventDefault()

            let info = {
                name, phone, address, country,email,
              }

              let data = {
                url:`${baseUrl}/contacts`,
                method: 'post',
                info, 
              }

        try {
        let result = await axios(data)
        console.log(result)
        } catch (error) {
            console.log("error aayo")
            console.log(error.message)
        }

        }}>

        
        <br></br>
        <label htmlFor='name' style={{paddingLeft:'5rem'}}> <b>Name:</b> </label>
        <input id='name'
               type='text'
               placeholder='Enter your name'
               value={name}
               onChange={(e) => {
                setName(e.target.value);
               }}
        ></input>


<br></br> <br></br><br></br> 
        <label htmlFor='phone' style={{paddingLeft:'5rem'}}><b>Phone:</b> </label>
        <input id='phone'
               type='number'
               placeholder='Enter your number'
               value={phone}
               onChange={(e) => {
                setPhone(e.target.value);
               }}
        ></input>


<br></br> <br></br><br></br> 
        <label htmlFor='address' style={{paddingLeft:'5rem'}}><b>Address:</b> </label>
        <input id='address'
               type='text'
               placeholder='Enter your address'
               value={address}
               onChange={(e) => {
                setAddress(e.target.value);
               }}
        ></input>


<br></br> <br></br><br></br> 
        <label htmlFor='Country' style={{paddingLeft:'5rem'}}><b>Country:</b> </label>
        <select id='Country'
                value={country}
                onChange={(e) => {
                    setCountry(e.target.value);
                }}>

{countries.map((item,i) => {
            return(
        <option key={i} 
                value={item.value} 
                disabled={item.disabled}>
                {item.label}
        </option>
            )
        })}
        
        </select>



        <br></br> <br></br><br></br> 
        <label htmlFor='email' style={{paddingLeft:'5rem'}}><b>Email:</b> </label>
        <input id='email'
               type='email'
               placeholder='Enter your email'
               value={email}
               onChange={(e) => {
                setEmail(e.target.value);
               }}
        ></input>
        <br></br> <br></br><br></br> 

        <button style={{ marginLeft: '9rem', marginBottom: '1rem' , cursor:"pointer" }} type='submit'>Sumbit Form</button>
        </form>

        </div>
    </>
  )
}

export default Practice