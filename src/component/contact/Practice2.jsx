import React, { useRef } from 'react'

const Practice2 = () => {

    let changeMyColor = useRef()
    let myAlso = useRef()
    let myToo = useRef()

  return (
    <div>
        <p ref={changeMyColor}>Changing my color</p>
        <p ref={myAlso}>My Also</p>
        <p ref={myToo}>My Too</p>

        <button
            onClick={() => {
                changeMyColor.current.style.backgroundColor = "brown"
                myAlso.current.style.backgroundColor = "aliceblue"
                myToo.current.style.backgroundColor = "green"

            }}
        >click</button>
    </div>
  )
}

export default Practice2