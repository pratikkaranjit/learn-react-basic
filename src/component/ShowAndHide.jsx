import React from 'react'
import { useState } from 'react';

const ShowAndHide = () => {
    let [show,setShow] = useState(true)
  return (
    <div>
        {show?<p>Error has occured</p>:null
}
    <button onClick={() => {
        setShow(false);
    }}>Hide</button>
<br></br>
    <button onClick={() => {
        setShow(true);
    }}>Show</button>
    </div>
  )
}

export default ShowAndHide