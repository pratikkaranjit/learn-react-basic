import React from 'react'
import { useState } from 'react'

const Counter = () => {
    let [count, setCount] = useState(0)
  return (
    <div>
        {count}
        <button onClick={() =>{
            setCount(count+1)
        }}>Increment</button>

        <button onClick={() => {
            setCount(count-1)
        }}>Decrement</button>

        <button onClick={() => {
            setCount(0) 
        }}>Reset</button>
    </div>
  )
}

export default Counter