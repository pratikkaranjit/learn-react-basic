import React from 'react'
import { NavLink, Route, Routes } from 'react-router-dom'
import Home from './Home'
import About from './About'
import Contact from './Contact'
import LearnDynamicRouteParameter from './LearnDynamicRouteParameter'
import ErrorPage from './ErrorPage'
import RoutesHere from './RoutesHere'

const LearnRoute = () => {
  return (
    <div>
        <NavLink to = "/home" style={{marginLeft:"20px"}}>Home</NavLink>
        <NavLink to = "/about" style={{marginLeft:"20px"}}>About</NavLink>
        <NavLink to = "/contact" style={{marginLeft:"20px"}}>Contact</NavLink>

        <RoutesHere></RoutesHere>
    </div>
  )
}

export default LearnRoute