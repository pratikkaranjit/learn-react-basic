import React from 'react'
import { Navigate } from 'react-router-dom'


const ErrorPage = () => {
  return (

        <Navigate to="/home"></Navigate>

  )
}

export default ErrorPage