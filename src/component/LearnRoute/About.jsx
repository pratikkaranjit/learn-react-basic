import React from 'react'
import { Navigate, useNavigate, useSearchParams } from 'react-router-dom'

const About = () => {

  let [searchParams] = useSearchParams();
  let Navigate = useNavigate();

  return (
    <div>
      About
      <br></br>
      {searchParams.get("name")}
      <br></br>
      {searchParams.get("address")}
      <br></br>
      {searchParams.get("age")}

      <button
      onClick={() => {
        Navigate("/contact",{replace:true})
      }}>
        Click to contact page
      </button>

      </div>
  )
}

export default About