import React from 'react'
import { Route, Routes } from 'react-router-dom'
import About from './About'
import Home from './Home'
import Contact from './Contact'
import ErrorPage from './ErrorPage'
import LearnDynamicRouteParameter from './LearnDynamicRouteParameter'

const RoutesHere = () => {
  return (
    <div>
    <Routes>

<Route path='/home' element={<Home></Home>}></Route>
<Route path='/about' element={<About></About>}></Route>     
<Route path='/contact' element={<Contact></Contact>}></Route>

{/* Tyo route parameter bahek aru kei input garyo bhane */}
<Route path='*' element={<ErrorPage></ErrorPage>}></Route>


{/* any ko thau ma j ni huna payo, if any ko thau ma already euta kei thiyo bhane tei page prefer garcha jastai eg:  */}
<Route path='/about/a1' element={<div>Any page</div>}></Route> 
{/* mathi ko example ma a1 prefer garcha even though tala hamle :any deko cha */}



{/* if path na bhako page lekhyo bhane :any define gardirakhne so that tyo page opens  */}
{/* /about is route parameter but :any is route parameter + dynamic  so dynamic huna : rakhnai paryo*/}
<Route path='/about/:any' element={<div>Any page</div>}></Route>



{/* in this we have used about ra pratik bhanne ani tyo chai same hunai paryo path ma tara :id1 ra :id2 deko thau ma aru j bhaye ni bhayo */}
<Route path='/about/:id1/pratik/:id2' element={<LearnDynamicRouteParameter></LearnDynamicRouteParameter>}></Route>


</Routes>

    </div>
  )
}

export default RoutesHere