import React from 'react'
import { useParams } from 'react-router-dom'

const LearnDynamicRouteParameter = () => {

  let params = useParams()
console.log(params)
  return (
    <div>
      LearnDynamicRouteParameter
      <br></br>
      {params.id1}
      <br></br>
      {params.id2}
    </div>
  )
}

export default LearnDynamicRouteParameter
