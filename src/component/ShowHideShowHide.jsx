import React from 'react'
import { useState } from 'react';

const ShowHideShowHide = () => {

    let [show,setShow] = useState(true)
  return (
    <div> 
        {show ?<p>This is a para</p>:null}
        <button onClick={() => {
            setShow(!show);
        }}>Show hide both</button>
    </div>
  )
}

export default ShowHideShowHide