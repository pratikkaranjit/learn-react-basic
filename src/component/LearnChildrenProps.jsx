import React from 'react'

const LearnChildrenProps = ({name,age,children}) => {
  return (
    <div style={{border:"5px solid"}}>LearnChildrenProps
        <br></br>
        {name}
        <br></br>
        {age}
        <br></br>
        <p className='bgColor'>{children}</p>

        <br></br>

    </div>
  )
}

export default LearnChildrenProps