import React, { useEffect, useState } from 'react'

const LearningUseFunction = () => {

    let [count,setCount] = useState(1)
    let [count1,setCount1] = useState(10)

    useEffect(() => {
        console.log("I am first")

        return() => {
            console.log("I am return")
        }
    },[count])
    
  return (
    <div>My Div
        <br></br>
        {count}
        <br></br>
        {count1}
        <button onClick={() => {
            setCount(count + 1)
        }}>Click to Increase</button>
        <button onClick={() => {
            setCount1(count1 - 1)
        }}>Click to Increase</button>
    </div>
  )
}

export default LearningUseFunction