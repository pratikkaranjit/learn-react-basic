import React, { useState } from 'react'

const Form5 = () => {

    let [email,setEmail] = useState("")
    let [number,setNumber] = useState("")
    let [isMarried,setIsMarried] = useState("")
    let [password,setPassword] = useState("")
    let [clothSize,setClothSize] = useState("")
    let [gender,setGender] = useState("")
    let [comment,setComment] = useState("")
    let [Role,setRole] = useState("")
    let [age,setAge] = useState("")


    let genders = [
        { label: "Male", value: "male"},
        { label: "Female", value: "female"},
        { label: "Other", value: "other"},
      ];

      let role = [
        {label:"Select Role", value:"",disabled: true},
        {label: "Admin", value:"admin"},
        {label: "Super Admin", value:"super admin"},
        {label: "Customer" , value:"customer"},
        {label: "DeliveryPerson", value:"delivery person"},
      ]



  return (
   <>
   <form
    onSubmit={(e) => {
        e.preventDefault()
        let info = {
            email,
            number,
            isMarried,
            password,
            clothSize,
            gender,
            comment,
            role,
            age,
        }
        console.log(info)
    }}>

        <br></br>
        <label htmlFor='email'>email: </label>
        <input
            id='name'
            type='email'
            placeholder='Enter your email'
            value={email}
            onChange={(e) => {
                setEmail(e.target.value);
            }}
        ></input>

<br></br>
        <label htmlFor='number'>number: </label>
        <input
            id='number'
            type='number'
            placeholder='Enter your number'
            value={number}
            onChange={(e) => {
                setNumber(e.target.value);
            }}
        ></input>


<br></br>
        <label htmlFor='password'>password: </label>
        <input
            id='password'
            type='password'
            placeholder='Enter your password'
            value={password}
            onChange={(e) => {
                setPassword(e.target.value);
            }}
        ></input>

<br></br>
        <label htmlFor='isMarried'>isMarried: </label>
        <input
            id='isMarried'
            type='checkbox'
            value={isMarried}
            onChange={(e) => {
                setIsMarried(e.target.checked);
            }}
        ></input>


<br></br>
        <label htmlFor='clothSize'>clothSize: </label>
        <input
            id='clothSize'
            type='number'
            value={clothSize}
            onChange={(e) => {
                setClothSize(e.target.value);
            }}
        ></input>

<br></br>
        
    <label htmlFor='male'>Gender: </label>
    {genders.map((item,i) => {
        return(
            <>
            <label htmlFor={item.value}>{item.label}</label>
            <input
            checked={gender === item.value}
            onChange={(e) => {
                setGender(e.target.value);
            }}
            type='radio'
            id = {item.value}
            value={item.value}
            ></input>
            </>
        )
    })}

<br></br>
        <label htmlFor='comment'>comment: </label>
        <textarea
            id='comment'
            placeholder='Enter your comment'
            rows = {5}
            cols={5}
            value={comment}
            onChange={(e) => {
                setComment(e.target.value);
            }}
        ></textarea>


<br></br>
        <label htmlFor='Role'>Role: </label>
        <select id='Role'
                value={Role}
                onChange={(e) => {
                    setRole(e.target.value)
                }}>

            {role.map((item,i) => {
                return(
                <option key={i} value={item.value}
                        disabled={item.disabled}
                >
                    {item.label}
                </option>
                )
            })}

                </select>

   </form>
   </>
  )
}

export default Form5