import React, { useState } from 'react'

const AdvancedUseState = () => {

    let [ count,setCount] = useState(0)

  return (
    <div>
        <button
            onClick={(pre) => {
                setCount((pre) => {
                    return count + 1;
                })
                setCount((pre) => {
                    return count + 1;
                })
                setCount((pre) => {
                    return count + 1;
                })
            }}
        >
            Click to change
        </button>
    </div>
  )
}

export default AdvancedUseState