import React, { useRef } from 'react'

const LearnUseRef = () => {

    let changeColor = useRef()
    let changeColor2 = useRef()
    let changeColor3 = useRef()

  return (
    <div style={{border:"1px solid", margin:"10rem", borderRadius:'30px'}}>
        <p ref={changeColor}>Change my color</p>
        <p ref={changeColor2}>change my color too</p>
        <input ref={changeColor3}></input>

        <button
            style={{cursor:"crosshair" , marginLeft:'3vh'}}
            onClick={() => {
                changeColor.current.style.backgroundColor ="pink"
                changeColor2.current.style.backgroundColor = "aliceblue"
                changeColor3.current.focus();
            }}
        >Click to change</button>

    </div>
  )
}

export default LearnUseRef