import React, { useState } from 'react'

//name
//address
//send

const Form2 = () => {
    let [name,setName] = useState("")
    let [address,setAddress] = useState("")
    let [password,setPassword] = useState("")
    let [description,setDescription] = useState("")
    let [occupation,setOccupation] = useState("")
    let [country,setCountry] = useState("")
    let [movie, setMovie] = useState("")
    let [gender,setGender] = useState("male")

    let countries = [
      { label: "Select Country", value: "", disabled: true },
      { label: "Nepal", value: "nepal" },
      { label: "China", value: "china" },
      { label: "India", value: "india" },
      { label: "America", value: "america" },
    ];

    let favouriteMoives = [
      {label:"Select Movies", value:"", disabled: true},
      {label: "The times", value:"the times"},
      {label: "Good", value:"good"},
      {label: "Day", value:"day"},
      {label: "Bad day", value:"bad day"},
      {label: "Super day", value:"super day"}
    ]

    let genders = [
      { label: "Male", value: "male"},
      { label: "Female", value: "female"},
      { label: "Other", value: "other"},
    ];

  return (
    <form 
    onSubmit={(e) => {
        e.preventDefault();
        let info = {
            name,
            address,
            password,
            description,
            occupation,
            country,
            movie,

        };
        console.log(info)
    }}>
        <br></br>

        <label htmlFor="name">Name: </label>
        <input id="name"
        type="text"
               placeholder = "Enter your name"
               value = {name}
               onChange={(e) => {
               setName(e.target.value);
            }
            }></input>
            <br></br>

            <label htmlFor="address">Address: </label>
            <input id="address"
                   type="text"
                   placeholder= "Enter your address"
                   value = {address}
                   onChange={(e) => {
                    setAddress(e.target.value);
                   }}
                   ></input>
                   <br></br>

                   <label htmlFor="Password">Password: </label>
                   <input 
                        id="password"
                        type="password"
                        placeholder='Enter your password'
                        value = {password}
                        onChange={(e) => {
                            setPassword(e.target.value);
                        }}
                    ></input>

                    <br></br>

                   <label htmlFor="description">description: </label>
                   <textarea 
                        id="description"
                        placeholder='Enter your description'
                        value = {description}
                        onChange={(e) => {
                            setDescription(e.target.value);
                        }}
                    ></textarea>
                    <br></br>
                   <label htmlFor='occupation'>Occupation: </label>
                   <input id="occupation" 
                          type = "occupation"
                          placeholder='Enter your occupation'
                          value = {occupation}
                          onChange={(e) => {
                            setOccupation(e.target.value);
                          }}
                    ></input>

                      <br></br>

                      
                  {/* <label htmlFor='country'>Country</label>
                          <br></br>

                  <select  id='country'
                          value = {country}
                          onChange={(e) => {
                            setCountry(e.target.value)
                          }}
                  >
                    <option value = "" disabled={true}>Select Country</option>
                    <option value = "nep">Nepal</option>
                    <option value = "chi">China</option>
                    <option value = "ind">India</option>
                    <option value = "ame">America</option>
                  </select> */}

                   <button type = "submit">Send</button>
                        

                    {/* //Array of objects deko cha ani teslai map method le select ra option nikaleko */}

                   {/* let countries = [
    { label: "Select Country", value: "", disabled: true },
    { label: "Nepal", value: "nepal" },
    { label: "China", value: "china" },
    { label: "India", value: "india" },
    { label: "America", value: "america" },
  ]; */}

<br></br>
<label htmlFor='country'>Country</label>
<br></br>
    <select>
    {countries.map((item,i) => {
      return (
      <option key ={i} value = {item.value} disabled={item.disabled}>
        {item.label}
         </option>
      )
    })}

    </select>


    <br></br>

  
      <br></br>
    <label htmlFor='Favourite Movies'> Favourite Movies</label>
    <select id = 'Favourite Movies'
    value= {movie}
            onChange={(e) => {
              setMovie(e.target.value)
            }}
            >

    {favouriteMoives.map((item,i) => {
      return(
      <option key ={i} value = {item.value} disabled={item.disabled}>
        {item.label}
      </option>
      )
    })}


    </select>

    <br></br>

    {/* <label>Gender</label>
    <br></br>

    <label htmlFor='male'>Male</label>
    <input id='male' 
    type='radio' 
    value="male" 
    checked={gender ==="male"} 
    onChange={(e) => {
      setGender(e.target.value)
    }}></input>

    <label htmlFor='female'>Female</label>
    <input id='female' 
    type='radio' 
    value="female"
    checked={gender ==="female"} 
    onChange={(e) => {
      setGender(e.target.value)
    }}
    ></input>

    <label htmlFor='other'>Other</label>
    <input id='other' 
    type='radio' 
    value="other"
    checked={gender === "other"} 
    onChange={(e) => {
      setGender(e.target.value)
    }}
    ></input> */}

    
    {/* //object bata code short pareko */}

    <label htmlFor="male">Gender </label>
      {genders.map((item, i) => {
        return (
          <>
            <label htmlFor={item.value}>{item.label} </label>
            <input
              checked={gender === item.value}
              onChange={(e) => {
                setGender(e.target.value);
              }}
              type="radio"
              id={item.value}
              value={item.value}
            ></input>
          </>
        );
      })}

    </form>
  )
}

export default Form2