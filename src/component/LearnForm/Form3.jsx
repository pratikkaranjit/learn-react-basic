import React, { useState } from 'react'

const Form3 = () => {

    let[name,setName] = useState("")
    let[age,setAge] = useState("")
    let[email,setEmail] = useState("")
    let[gender,setGender] = useState("")

    let genders = [
        { label: "Male", value: "male"},
        { label: "Female", value: "female"},
        { label: "Other", value: "other"},
      ];

  return (
    <form 
        onSubmit={(e) => {
            e.preventDefault();
            let info = {
                name,
                age,
                email,
                gender,
            }
            console.log(info)
        }}>
        <br></br>

        <label htmlFor='name'>Name: </label>
        <input id='name'
               type='text'
               placeholder='Enter your name'
               value={name}
               onChange={(e) => {
                setName(e.target.value);
               }}
        ></input>

        <br></br>
        <label htmlFor='age'>Age: </label>
        <input id='age'
               type='number'
               placeholder='Enter your age'
               value={age}
               onChange={(e) => {
                setAge(e.target.value);
               }}
        ></input>

        <br></br>
        <label htmlFor='email'>Email: </label>
        <input id='email'
               type='email'
               placeholder='Enter your email address'
               value={email}
               onChange={(e) => {
                setEmail(e.target.value);
               }}
        ></input>

        <br></br>
        <label htmlFor='gender'>gender: </label>
        {genders.map((item,i) => {
            return (
                <>
                <label htmlFor={item.value}>{item.label}</label>
                <input
                checked={gender === item.value}
                onChange={(e) => {
                    setGender(e.target.value)
                }}
                type='radio'
                id = {item.value}
                value={item.value}
                ></input>
                </>
            )
        })}

        <br></br>
    


    </form>
  )
}

export default Form3