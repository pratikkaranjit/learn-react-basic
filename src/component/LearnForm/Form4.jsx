import React, { useState } from 'react'

const Form4 = () => {

    let [name,SetName] = useState("")
    let [email,setEmail] = useState("")
    let [staffEmail, setStaffEmail] = useState("")
    let [password,setPassword] = useState("")
    let [gender, setGender] = useState("")
    let [date,setDate] = useState("")
    let [isMarried,setIsMarried] = useState("")
    let [spouse,setSpouse] = useState("")
    let [location,setLocation] = useState("")
    let [description,setDescription] = useState("")
    let [isAvailable,setIsAvailable] = useState("")




    let genders = [
        { label: "Male", value: "male"},
        { label: "Female", value: "female"},
        { label: "Other", value: "other"},
      ];


  return (

    <form
    onSubmit={(e) => {
        e.preventDefault();
        let info = {
            name,
            email,
            staffEmail,
            password,
            gender,
            date,
            isMarried,
            spouse,
            location,
            description,
            isAvailable,
        }     
        console.log(info)
    }}
    >
    <br></br>


    <label htmlFor='Product Name'>Product Name: </label>
    <input 
    id = 'productName'
    type='text'
    placeholder='Enter the name of the product'
    value={name}
    onChange={(e) => {
        SetName(e.target.value);
    }}

    ></input> <br></br>
    <label htmlFor='Product Manager Email'>Product Manager Email: </label>
    <input 
    id = 'Product Manager Email'
    type='email'
    placeholder='Enter the Product Manager Email'
    value={email}
    onChange={(e) => {
        setEmail(e.target.value);
    }}

    ></input>
    <br></br>

    <label htmlFor='Staff Email'>Staff Email: </label>
    <input 
    id = 'Staff Email'
    type='email'
    placeholder='Enter the Staff Email'
    value={staffEmail}
    onChange={(e) => {
        setStaffEmail(e.target.value);
    }}

    ></input>

<br></br>

    <label htmlFor='password'>password: </label>
    <input 
    id = 'password'
    type='password'
    placeholder='Enter the password'
    value={password}
    onChange={(e) => {
        setPassword(e.target.value);
    }}

    ></input>
<br></br>
    <label htmlFor='gender'>Gender: </label>
    {genders.map((item,i) => {
        return (
            <>
        <label htmlFor={item.value}>{item.label}</label>
        <input
        checked={gender === item.value}
        onChange={(e) => {
            setGender(e.target.value);
        }}
        type='radio'
        id={item.value}
        value={item.value}
        ></input>
        </>
        )
    })}

<br></br>
<label htmlFor='date'>date: </label>
    <input 
    id = 'date'
    type='date'
    value={date}
    onChange={(e) => {
        setDate(e.target.value);
    }}
    ></input>

<br></br>
<label htmlFor='isMarried'>isMarried: </label>
    <input 
    id = 'isMarried'
    type='checkbox'
    value={isMarried}
    onChange={(e) => {
        setIsMarried(e.target.checked);
    }}

    ></input>


<br></br>
<label htmlFor='managerSpouse'>managerSpouse: </label>
    <input 
    id = 'managerSpouse'
    type='text'
    placeholder='Enter spouse name'
    value={spouse}
    onChange={(e) => {
        setSpouse(e.target.value);
    }}

    ></input>


<br></br>
<label htmlFor='productLocation'>productLocation: </label>
    <input 
    id = 'productLocation'
    type='text'
    placeholder='Enter product Location'
    value={location}
    onChange={(e) => {
        setLocation(e.target.value);
    }}

    ></input>


<br></br>
<label htmlFor='productDescription'>productDescription: </label>
    <textarea
    id = 'productDescription'
    type='text'
    placeholder='Enter product description'
    value={description}
    onChange={(e) => {
       setDescription(e.target.value);
    }}

    ></textarea>


<br></br>
<label htmlFor='isAvailable'>isAvailable: </label>
    <input 
    id = 'isAvailable'
    type='checkbox'
    value={isAvailable}
    onChange={(e) => {
        setIsAvailable(e.target.checked);
    }}

    ></input>


    </form>
  )
}

export default Form4