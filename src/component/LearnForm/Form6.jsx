import React, { useState } from 'react'

const Form6 = () => {

    let [productName,setProductName] = useState("")
    let [ productPrice, setProductPrice] = useState("")
    let [ productQuantity, setProductQuantity] = useState("")
    let [ productCategory, setProductCategory] = useState("")


    // // let category = [
    // //     {label:}
    // ]

  return (

    <form
    onSubmit={(e) => {
        e.preventDefault()
        let info = {
            productName,
            productPrice,
            productQuantity,
            productCategory,
        }
    }}
    >

    <br></br>
    <label htmlFor='productName'>Product Name: </label>
    <input
        id='productName'
        type='text'
        placeholder='Enter product name'
        value={productName}
        onChange={(e) => {
            setProductName(e.target.value);
        }}
    ></input>

<br></br>
    <label htmlFor='productPrice'>Product Price: </label>
    <input
        id='productPrice'
        type='number'
        placeholder='Enter Product Price'
        value={productPrice}
        onChange={(e) => {
            setProductPrice(e.target.value);
        }}
    ></input>

<br></br>
    <label htmlFor='productQuantity'>Product Quantity: </label>
    <input
        id='productQuantity'
        type='number'
        placeholder='Enter Product Quantity'
        value={productQuantity}
        onChange={(e) => {
            setProductQuantity(e.target.value);
        }}
    ></input>



    </form>
  )
}

export default Form6