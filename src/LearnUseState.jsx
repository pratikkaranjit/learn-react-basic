import React from 'react'
import { useState } from 'react'

const LearnUseState = () => {
    
    let [count1,setCount1,a] = useState(0)
    console.log("I am Nitan")
    function incremental(){
      count1 = count1+1;
      return count1
    }
  return (
    <div>
        {count1}  
        <br></br>

    <button onClick={() =>{
      setCount1(incremental())
    }}>Click me to change the number</button>
    </div>
  )
}

export default LearnUseState