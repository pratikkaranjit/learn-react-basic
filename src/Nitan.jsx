import React, { useState } from 'react'
import Pratik from './Pratik'
import LearnUseState from './LearnUseState'
import Counter from './component/Counter'
import ShowAndHide from './component/ShowAndHide'
import ShowHideShowHide from './component/ShowHideShowHide'
import LearnUseEffect from './component/LearnUseEffect'
import LearnUseEffect2 from './component/LearnUseEffect2'
import LearnChildrenProps from './component/LearnChildrenProps'
import LearningUseFunction from './component/LearningUseFunction'
import Form1 from './component/LearnForm/Form1'
import Form2 from './component/LearnForm/Form2'
import Form3 from './component/LearnForm/Form3'
import Form4 from './component/LearnForm/Form4'
import Form5 from './component/LearnForm/Form5'
import Form6 from './component/LearnForm/Form6'
import Form7 from './component/contact/Form7'
import HitApi1 from './component/learnAPI/HitApi1'
import ReadAllContact from './component/contact/ReadAllContact'
import Practice from './component/contact/Practice'
import AdvancedUseState from './component/LearnForm/AdvancedUseState'
import LearnUseRef from './component/LearnForm/LearnUseRef'
import Practice2 from './component/contact/Practice2'
import LearnRoute from './component/LearnRoute/LearnRoute'

const Nitan = () => {

  //ShowComp for and operator

  let[showComponent,setShowComponent] = useState(true);

   
  //bestfrined ko naam nikalne using map method 


  let myBestFriend = ["nitan","ram","shyam"]


  //multiple options 

  let _age = 16
  let message = _age === 16?"Your age is 16": _age === 17?"Your age is 17": _age === 18?"Your age is 18":"Your age is neither 16,17,18"
  console.log(message)


  //Homework

  let ar1=[
    {
    name:"nitan",
    gender:"male",
    },
    {
    name:"sita",
    gender:"female",
    },
    {
    name:"hari",
    gender:"male",
    },
    {
    name:"gita",
    gender:"female",
    },
    {
    name:"utshab",
    gender:"other",
    },
    ]




  //declaring for products, electronics and price 


  let products = [
    {
      id: 1,
      title: "Product 1",
      category: "electronics",
      price: 5000,
      description: "This is description and Product 1",
      discount: {
        type: "other",
      },
    },
    {
      id: 2,
      title: "Product 2",
      category: "cloths",
      price: 2000,
      description: "This is description and Product 2",
      discount: {
        type: "other",
      },
    },
    {
      id: 3,
      title: "Product 3",
      category: "electronics",
      price: 3000,
      description: "This is description and Product 3",
      discount: {
        type: "other",
      },
    },
  ];


//Ternary Operator


  let age = 13
  let canWatchMovies = age>= 18?"You can watch movies":"You cannot watch movies"
  console.log(canWatchMovies)

  // let mySpace = 
  // [{
  //     id: 1,
  //     title: "Product 1",
  //     category: "electronics",
  //     price: 5000,
  //     description: "This is description and Product 1",
  //     discount: {
  //       type: "other",
  //     },
  //   },
  //   {
  //     id: 2,
  //     title: "Product 2",
  //     category: "cloths",
  //     price: 2000,
  //     description: "This is description and Product 2",
  //     discount: {
  //       type: "other",
  //     },
  //   },
  //   {
  //     id: 3,
  //     title: "Product 3",
  //     category: "electronics",
  //     price: 3000,
  //     description: "This is description and Product 3",
  //     discount: {
  //       type: "other",
  //     },
  //   },
  // ];

  // let myVar = mySpace.sort((a,b) => {
  //     return a.id - b.id;
  // })
  // console.log(myVar)
  

  return (
    <div>
        {/* <Pratik naam ="Pratik" age = "20" address = "Ktm" phone = "1313213123"></Pratik>  
        <p style={{backgroundColor:"red",color:"white",width:"20rem",height:"5rem",margin:"10rem 10rem 0rem 10rem",textAlign:"center",border:"10px solid black",paddingTop:"2rem"}}>This is a paragraph</p> 
        <button onClick={()=>{
          console.log("Button is clicked")
        }} style={{backgroundColor:"grey",cursor:"crosshair"}}>Click Me!</button>   

        {myBestFriend.map((value,i) =>{
          return <p key = {i}>my best friend is {value} </p>

        })}
            
        {products.map((value,i) =>{
          return <div key={i}><p style={{backgroundColor:"pink"}}>{value.title} is {value.category} and it costs {value.price} </p></div>
        })
      } */}

      {/* Finding the age */}
{/* 
      {_age === 11?
      <p>Your age is 16</p>
      :age === 17?
      <p>Your age is 17</p>
      :age === 18?
      <p>Your age is 18</p>
      :<p>Your age is neither 16 17 18</p>
      }
    
    <div>
    </div> */}
  
    {/* <LearnUseState></LearnUseState>
    <Counter></Counter> */}
    {/* <ShowAndHide></ShowAndHide>
    <ShowHideShowHide></ShowHideShowHide>
    <LearnUseState></LearnUseState> */}
    {/* <LearnUseEffect></LearnUseEffect> */}

    {/* {showComponent && <LearnUseEffect2></LearnUseEffect2>} */}
    {/* <button onClick = {() => {

      //!showComponent le press garda hide garcha then again press garda show huncha 
      // setShowComponent(!showComponent);          
    }}
    // >Click to hide</button> */}

    {/* <LearnChildrenProps name={'nitan'} age={10}>This is children props</LearnChildrenProps> */}
    {/* <LearningUseFunction></LearningUseFunction> */}
    {/* <Form1></Form1> */}
    {/* <Form2></Form2> */}

    {/* <Form3></Form3> */}

    {/* <Form4></Form4> */}

    {/* <Form5></Form5> */}

    {/* <Form6></Form6> */}

    {/* <Form7></Form7> */}

    {/* <HitApi1></HitApi1> */}

    {/* <ReadAllContact></ReadAllContact> */}

    {/* <Practice></Practice> */}

    {/* <AdvancedUseState></AdvancedUseState> */}

    {/* <LearnUseRef></LearnUseRef>*/}

    {/* <Practice2></Practice2> */}

    <LearnRoute></LearnRoute>

    </div>
  )
}


export default Nitan

