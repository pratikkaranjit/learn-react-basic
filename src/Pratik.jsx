import React from 'react'

const Pratik = ({naam,age,address,phone}) => {
  return (
    <div>
        <p>My naam is {naam}</p>
        <p>My address is {address}</p>
        <p>My age is {age}</p>
        <p>My phone is {phone}</p>
    </div>
  )
}

export default Pratik